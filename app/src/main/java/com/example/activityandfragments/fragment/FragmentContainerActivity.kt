package com.example.activityandfragments.fragment

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.activityandfragments.R
import com.example.activityandfragments.databinding.FragmentContainerMainBinding

class FragmentContainerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = FragmentContainerMainBinding.inflate(layoutInflater)

        with (supportFragmentManager.beginTransaction()) {
            add(binding.fragmentDynamic.id, HelloWorldFragment())
            commit()
        }

        binding.showDialogButton.setOnClickListener {
            MessageDialogFragment().show(supportFragmentManager, "Arbitrary tag")
        }

        val testFragmentFound: (Fragment?, String) -> Unit = {fragment, name ->
            if (fragment == null) {
                Toast.makeText(this, "$name fragment not found.", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "$name fragment found.", Toast.LENGTH_SHORT).show()
            }
        }

        testFragmentFound(supportFragmentManager.findFragmentById(R.id.fragmentStatic), "Static")
        testFragmentFound(supportFragmentManager.findFragmentById(R.id.fragmentDynamic), "Dynamic")

        setContentView(binding.root)
    }
}