package com.example.activityandfragments.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.activityandfragments.databinding.HelloWorldFragmentBinding

class HelloWorldFragment : Fragment() {

    private lateinit var binding: HelloWorldFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HelloWorldFragmentBinding.inflate(layoutInflater)
        return binding.root
    }
}