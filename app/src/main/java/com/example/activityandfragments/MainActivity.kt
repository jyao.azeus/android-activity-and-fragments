package com.example.activityandfragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import com.example.activityandfragments.activity.ActivityLauncher
import com.example.activityandfragments.databinding.ActivityMainBinding
import com.example.activityandfragments.databinding.ExamplesListItemBinding
import com.example.activityandfragments.fragment.FragmentContainerActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mainBinding = ActivityMainBinding.inflate(layoutInflater)

        mainBinding.list.apply {
            adapter = ExamplesAdapter { view ->
                getChildAdapterPosition(view)
                    .takeUnless { it == NO_POSITION }
                    ?.let {
                        navigateTo(Examples.values()[it])
                    }
            }
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        setContentView(mainBinding.root)
    }

    private fun navigateTo(example: Examples) = when (example) {
        Examples.Activity -> {
            val intent = Intent(this, ActivityLauncher::class.java)
            this.startActivity(intent)
        }
        Examples.Fragment -> {
            val intent = Intent(this, FragmentContainerActivity::class.java)
            this.startActivity(intent)
        }
    }

    enum class Examples(val label: String) {
        Activity("Activity intent sample"), Fragment("Fragment transaction sample")
    }

    class ExamplesViewHolder(itemView: View,
                             val binding: ExamplesListItemBinding) : RecyclerView.ViewHolder(itemView)

    class ExamplesAdapter(private val onClickListener: OnClickListener) : Adapter<ExamplesViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamplesViewHolder {
            val itemBinding = ExamplesListItemBinding.inflate(LayoutInflater.from(parent.context))
            itemBinding.root.setOnClickListener(onClickListener)

            return ExamplesViewHolder(itemBinding.root, itemBinding)
        }

        override fun getItemCount(): Int = Examples.values().size

        override fun onBindViewHolder(holder: ExamplesViewHolder, position: Int) {
            holder.binding.text1.text = Examples.values()[position].label
        }

    }
}