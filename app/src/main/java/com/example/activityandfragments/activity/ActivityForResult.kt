package com.example.activityandfragments.activity

import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity

class ActivityForResult : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        LinearLayout(this).apply {
            orientation = LinearLayout.VERTICAL

            val editText = EditText(this@ActivityForResult).apply {
                hint = "Enter value to return"
                layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            }.also { addView(it) }

            Button(this@ActivityForResult).apply {
                text = "Finish activity"
                layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                setOnClickListener {
                    val data = Intent()
                    data.putExtra(ActivityLauncher.RESULT_KEY, editText.text.toString())
                    this@ActivityForResult.setResult(1, data)
                    this@ActivityForResult.finish()
                }
            }.also { addView(it) }

        }.also {
            setContentView(it)
        }
    }
}