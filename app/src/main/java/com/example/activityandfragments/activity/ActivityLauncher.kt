package com.example.activityandfragments.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class ActivityLauncher : AppCompatActivity() {
    companion object {
        const val REQUEST_CODE = 1
        const val RESULT_KEY = "KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val content = Button(this).apply {
            text = "Launch activity"
            setOnClickListener {
                val intent = Intent(this@ActivityLauncher, ActivityForResult::class.java)
                this@ActivityLauncher.startActivityForResult(intent, REQUEST_CODE)
            }
        }

        setContentView(content)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE -> {
                Toast.makeText(this, "Value received is ${data?.getStringExtra(RESULT_KEY)}", Toast.LENGTH_SHORT).show()
            }
        }
    }
}